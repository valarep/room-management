﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableLayerDemo.Classes;
using TableLayerDemo.DAL;

namespace TableLayerDemo.DAO
{
    public static class RoomDao
    {
        public static List<Room> GetAll()
        {
            List<Room> items = new List<Room>();
            string commandText = "SELECT * FROM `room`;";
            MySqlConnection connection = AbstractDAO.Connection;
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = commandText;
            command.Prepare();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Room
                {
                    Id = (int)reader["id"],
                    Name = (string)reader["name"],
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
