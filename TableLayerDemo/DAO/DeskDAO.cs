﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableLayerDemo.Classes;
using TableLayerDemo.DAL;

namespace TableLayerDemo.DAO
{
    public static class DeskDAO
    {
        public static List<Desk> GetAll(Room room)
        {
            List<Desk> items = new List<Desk>();
            string commandText = "SELECT * FROM `desk` " +
                                 "WHERE `room_id` = ?room_id;";
            MySqlConnection connection = AbstractDAO.Connection;
            connection.Open();
            MySqlCommand command = connection.CreateCommand();
            command.CommandText = commandText;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("room_id", room.Id));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                items.Add(new Desk
                {
                    Id = (int)reader["id"],
                    Number = (int)reader["number"],
                    Size = new Size
                    {
                        Width = (int)reader["size_width"],
                        Height = (int)reader["size_height"],
                    },
                    Location = new Point
                    {
                        X = (int)reader["location_x"],
                        Y = (int)reader["location_y"],
                    },
                });
            }
            reader.Close();
            connection.Close();
            return items;
        }
    }
}
