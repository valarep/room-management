﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableLayerDemo.DAO;

namespace TableLayerDemo.Classes
{
    public class Desk
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public Size Size { get; set; }
        public Point Location { get; set; }
        public Room Room { get; set; }
        public List<Computer> Computers { get; set; }
        public static List<Desk> GetAll(Room room) => DeskDAO.GetAll(room);
    }
}
