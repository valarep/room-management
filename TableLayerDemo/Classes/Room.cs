﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableLayerDemo.DAO;

namespace TableLayerDemo.Classes
{
    public class Room
    {
        public int Id { get; set; }
        public string Name { get; set; }
        private List<Desk> desks;
        public List<Desk> Desks
        {
            get
            {
                if (desks == null)
                {
                    desks = Desk.GetAll(this);
                }
                return desks;
            }
        }
        public static List<Room> GetAll() => RoomDao.GetAll();
    }
}
