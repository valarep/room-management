﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableLayerDemo.Classes
{
    public class Computer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Point Location { get; set; }
        public Desk Desk { get; set; }
    }
}
