﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TableLayerDemo.DAL
{
    public static class AbstractDAO
    {
        private static MySqlConnectionStringBuilder connexionStringBldr = new MySqlConnectionStringBuilder
        {
            Server = ConfigurationManager.AppSettings.Get("Server"),
            UserID = ConfigurationManager.AppSettings.Get("UserID"),
            Password = ConfigurationManager.AppSettings.Get("Password"),
            Database = ConfigurationManager.AppSettings.Get("Database"),
            CharacterSet = ConfigurationManager.AppSettings.Get("CharacterSet"),
        };
        public static MySqlConnection Connection { get; } = new MySqlConnection
        {
            ConnectionString = connexionStringBldr.ConnectionString
        };
    }
}
